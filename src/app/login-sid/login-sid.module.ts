import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginSidPageRoutingModule } from './login-sid-routing.module';

import { LoginSidPage } from './login-sid.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginSidPageRoutingModule
  ],
  declarations: [LoginSidPage]
})
export class LoginSidPageModule {}
