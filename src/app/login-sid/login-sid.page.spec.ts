import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginSidPage } from './login-sid.page';

describe('LoginSidPage', () => {
  let component: LoginSidPage;
  let fixture: ComponentFixture<LoginSidPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginSidPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginSidPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
