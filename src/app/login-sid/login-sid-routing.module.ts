import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginSidPage } from './login-sid.page';

const routes: Routes = [
  {
    path: '',
    component: LoginSidPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginSidPageRoutingModule {}
