import { Component, OnInit } from '@angular/core';
import {Account, NetworkType, Listener} from 'tsjs-xpx-chain-sdk';
import { ApiNode, VerifytoLogin, CredentialRequired, LoginRequestMessage } from 'siriusid-sdk';
import {AdminDataService} from 'src/service/admin-data.service'; 
import {ClientInfoService} from 'src/service/client-info.service'; 
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login-sid',
  templateUrl: './login-sid.page.html',
  styleUrls: ['./login-sid.page.scss'],
})
export class LoginSidPage {
  currentNode: string = "api-1.testnet2.xpxsirius.io";
  listener: Listener;
  connect: boolean;
  warning: boolean;
  subscribe: any;
  credential;
  credential_id = "siriusId";
  showImg: boolean;
  img;


  constructor(
    private adminDataService:AdminDataService,
    public alertController: AlertController,
    private router: Router,
    private clientInfo:ClientInfoService
  ) { 
    console.log('Api Node: ' + this.currentNode);
  }

  ionViewWillEnter(){
    ApiNode.apiNode = "https://" + this.currentNode;
    ApiNode.networkType = NetworkType.TEST_NET;
    this.adminDataService.apiNode = this.currentNode;
    this.adminDataService.updateWebsocket();
    this.listener = new Listener(this.adminDataService.ws, WebSocket);
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Admin Page will ask you to share',
      message: '<div class="alert"><img src="assets/proximax-siriusid-navbar-credentials-enable-blue-h100.svg"><p>Default Credential</p></div>',
      buttons: [
        {
          text: 'Login' ,
          cssClass: 'primary',
          handler: () => {
            this.loginRequestAndVerify();
          }
        }
      ],
      cssClass:"boxAlert"
    });

    await alert.present();
  }

  async loginRequestAndVerify(){
    this.connect = false;
    this.credential = CredentialRequired.create(this.credential_id);
    const loginRequestMessage = LoginRequestMessage.create(this.adminDataService.publicKeydApp,[this.credential]);
    const sessionToken = loginRequestMessage.getSessionToken();
    this.img = await loginRequestMessage.generateQR();
    // this.linkLogin = await loginRequestMessage.universalLink();

    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0  && !this.connect){
        this.warning = true;
        this.listener = new Listener(this.adminDataService.ws, WebSocket);
        this.connection(sessionToken);
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg = true;
      }
      else if(this.connect){
        clearInterval(interval);
      }
    }, 1000);
  }
  
  async connection(sessionToken) {
    
    this.listener.open().then(() => {
      this.warning = false;
      this.subscribe = this.listener.confirmed(this.adminDataService.addressdApp).subscribe(transaction => {
        VerifytoLogin.verify(transaction, sessionToken,[this.credential],this.adminDataService.privateKeydApp).then(
          verify=>{
            if (verify){
              this.connect = true;
              console.log("Transaction matched");          
              let message = VerifytoLogin.getMessage();
              console.log(message);
              this.clientInfo.clientAddress = transaction.signer.address.plain();
              this.clientInfo.client = true;
              console.log(this.adminDataService.addressSiriusid);
              this.subscribe.unsubscribe();
              this.router.navigate(['/home']);
            }
            else console.log("Transaction not match");    
          }
        )
             
      });
    })
    
  }
}
