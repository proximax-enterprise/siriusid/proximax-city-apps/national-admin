export interface User {
  uid: string;
  email: string;
  displayName: string;
  privateKey: string;
  namespace: string;
  emailVerified: boolean;

}