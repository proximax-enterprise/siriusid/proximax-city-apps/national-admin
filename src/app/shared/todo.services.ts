import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Address,TransactionHttp, Transaction, Listener} from 'tsjs-xpx-chain-sdk';
 
export interface Todo {
  id: string;
  dAppId:string;
  createdAt: number;
  userData: string;
  address: string;
  publicKey:string;
  approved:boolean;
}
 

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  privateKeydAppAdmin = 'C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAAA'
  generationHash = "031248ED76E99807C198D2A219EEBBE678D78B036D92BA505988994356DF61B8";
  apiNode = "api-1.testnet2.xpxsirius.io";
  ws = 'wss://' + this.apiNode;
  listener;
  

  private todosCollection: AngularFirestoreCollection<Todo>;
 
  private todos: Observable<Todo[]>;
 
  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Todo>('todos');
 
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getTodos() {
    return this.todos;
  }
 
  getTodo(id) {
    return this.todosCollection.doc<Todo>(id).valueChanges();
  }
 
  updateTodo(todo: Todo, id: string) {
    return this.todosCollection.doc(id).update(todo);
  }
 
  addTodo(todo: Todo) {
    return this.todosCollection.add(todo);
  }
 
  removeTodo(id) {
    return this.todosCollection.doc(id).delete();
  }

  check() {
    if (!this.listener) return false;
    const listenerAsAny = this.listener as any;
    if (!listenerAsAny.webSocket) return false;
    return listenerAsAny.webSocket.readyState === 1;
  }

  open() {
    this.listener = new Listener(this.ws);
    return this.listener.open();
  }

  async checkAndOpenListener() {
  if (!this.check()) await this.open();
  }

  async waitForTransactionConfirmed(address: Address, signedTxHash: string, fnAnnounce?: any, timeoutPolling: number = 30000) {
    await this.checkAndOpenListener();
    console.log('this.checkAndOpenListener()',this.checkAndOpenListener());
    return new Promise<Transaction>((resolve, reject) => {
        const statusSub = this.listener.status(address)
            .subscribe(
                status => {
                    clearTimeout(timeoutListener);
                    reject(status);
                    confirmSub.unsubscribe();
                    statusSub.unsubscribe();
                },
                error => { reject(error.status); },
                () => { console.log('[Listener] Status Listener Done'); }
            );

        const confirmSub = this.listener.confirmed(address)
            .subscribe(
                successTx => {
                    if (successTx && successTx.transactionInfo && successTx.transactionInfo.hash === signedTxHash) {
                        clearTimeout(timeoutListener);
                        resolve(successTx);
                        confirmSub.unsubscribe();
                        statusSub.unsubscribe();
                    }
                },
                error => { reject(error.status); },
                () => { console.log('[Listener] Confirmed Listener Done.'); }
            );
        fnAnnounce();//

        const timeoutListener = setTimeout(async () => {
            const transactionHttp = new TransactionHttp("https://"+this.apiNode);
            const status = await transactionHttp.getTransactionStatus(signedTxHash).toPromise();

            if (status.group == 'unconfirmed') {
                const statusPolling = setInterval(async () => {
                    const statusReget = await transactionHttp.getTransactionStatus(signedTxHash).toPromise();
                    if (statusReget.group == 'confirmed') {
                        const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                        resolve(tx);
                        clearInterval(statusPolling);
                    }
                    if (statusReget.group == 'failed') {
                        reject(statusReget);
                        clearInterval(statusPolling);
                    }
                }, 500);
            }

            if (status.group == 'confirmed') {
                const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                resolve(tx);
            }

            if (status.group == 'failed') {
                reject(status);
            }
        }, timeoutPolling);
    });
  }

  
}