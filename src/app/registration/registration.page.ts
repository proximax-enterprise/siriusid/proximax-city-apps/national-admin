import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "../shared/authentication-service";
import {AlertController} from '@ionic/angular';
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})

export class RegistrationPage implements OnInit {
  start = true;
  step0 = false;
  step1 = false;
  step2 = false;
  userData:any;

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public alertController:AlertController,
    public ngFireAuth: AngularFireAuth,
  ) {
    console.log("registration page");
    this.authService.start();
   }

  ngOnInit(){ }

  async presentAlertNeedVerificationEmail(){
    const alert = await this.alertController.create({
      header:"Notification:",
      subHeader:"E-mail verification needed",
      message:'Your e-mail address must be verified before logging in.',
      buttons:["OK"]
    });
    await alert.present();
  }

  async presentAlertNeedInfo(){
    const alert = await this.alertController.create({
      header:"Notification:",
      subHeader:"Lack of Information",
      message:'Please fill in form before logging in.',
      buttons:["OK"]
    });
    await alert.present();
  }

  signUpForm(){
    this.step0 = true;
    this.start = false;
    this.step1 = false;
    this.step2 = false;
  }

  logInForm(){
    console.log('this.authService',this.authService)
    this.step0 = false;
    this.start = false;
    this.step1 = false;
    this.step2 = true;
  }

  resetForm(){
    this.start = true;
    this.step0 = false;
    this.step1 = false;
    this.step2 = false;
  }

  async signUp(email, password){
    this.authService.RegisterUser(email.value, password.value)      
    .then(() => {
      this.authService.sendVerificationMail()
      this.router.navigate(['registration']);
      this.step1 = true;
      this.step0 = false;
      let interval = setInterval(async()=>{
        this.ngFireAuth.authState.subscribe((result)=>{
            result.reload().then(
              ()=>{
              if(result.emailVerified==true){
                clearInterval(interval);
                this.start = true;
                this.step1 = false;
                this.router.navigate(['home'])
              }
              }
            )
          });
      },2000);
    }).catch((error) => {
      window.alert(error.message)
    })
  }

  async logIn(emailLogIn,passwordLogIn) {
    if(emailLogIn.value && passwordLogIn.value){
      this.authService.SignIn(emailLogIn.value, passwordLogIn.value)
        .then((res) => {
          if(this.authService.isEmailVerified) {
            this.router.navigate(['home']); 
            this.resetForm();       
          } else {
            window.alert('Email is not verified')
            return false;
          }
        }).catch((error) => {
          window.alert(error.message)
      })
    } else{
      this.presentAlertNeedInfo()
    }
  } 
}