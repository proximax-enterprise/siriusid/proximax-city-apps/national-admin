import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../shared/authentication-service";
import {Todo,TodoService} from '../shared/todo.services';
import {ActivatedRoute} from '@angular/router';
import { NavController,LoadingController} from '@ionic/angular';
import {CredentialResponseMessage, Credentials} from 'siriusid-sdk';
import { PublicAccount, EncryptedMessage, TransferTransaction, Deadline, UInt64, NetworkType, Address, Account,TransactionHttp,TransactionBuilderFactory } from 'tsjs-xpx-chain-sdk';
import {
  BlockchainNetworkConnection,
  ConnectionConfig,
  Protocol,
  IpfsConnection,
  BlockchainNetworkType,
  DownloadParameter,
  Downloader,
  Uploader,
  Uint8ArrayParameterData,
  UploadParameter,
} from "tsjs-chain-xipfs-sdk";
import { Certificate } from 'crypto';
import { fromEventPattern } from 'rxjs';
import { element } from 'protractor';
import { ClientInfoService } from 'src/service/client-info.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  isShowDetail = false;
  selectedCredential = {
    firstName: '',
    lastName: '',
    dateOfBirth: '',
    address: '',
    phone: '',
    email: '',
    motherName: '',
    fatherName: '',
    photoHash: '',
    photo: ''
  }
  todo: Todo;
  userAddress;
  photonew;
  publicKey;
  sessionToken;
  transaction: TransferTransaction;
  readonly credentialFields = [
    'firstName',
    'lastName',
    'dateOfBirth',
    'address',
    'phone',
    'email',
    'motherName',
    'fatherName',
    'photo'
  ];
  readonly credentialFieldText = {
    firstName: 'First name',
    lastName: 'Last name',
    dateOfBirth: 'Date of birth',
    address: 'Address',
    phone: 'Phone number',
    email: 'Email',
    motherName: 'Mother\'s name',
    fatherName: 'Father\'s name',
    photo: 'Photo'
  }
  pendingList: Todo[];
  approvedList: Todo[];
  activeItemIndex = -1;
  todoId = null;
  payloadContent;
  BlockchainInfo = {
    apiHost: 'api-1.testnet2.xpxsirius.io',
    apiPort: 443,
    apiProtocol: 'https'
  };
  IpfsInfo = {
    host: 'ipfs1-dev.xpxsirius.io',
    options: { protocol: 'https' },
    port: 5443
  };
  pending = true;

  connectionConfig = ConnectionConfig.createWithLocalIpfsConnection(
    new BlockchainNetworkConnection(
      BlockchainNetworkType.TEST_NET,
      this.BlockchainInfo.apiHost,
      this.BlockchainInfo.apiPort,
      Protocol.HTTPS
    ),
    new IpfsConnection(this.IpfsInfo.host, this.IpfsInfo.port,this.IpfsInfo.options)
  );

  constructor(
    // private notificationService: NotificationService,
    private route: ActivatedRoute,
    public authService: AuthenticationService,
    private todoService: TodoService,
    private loadingController: LoadingController,
    private clientInfo:ClientInfoService
  ) { }

  ngOnInit() {
    this.todoId = this.route.snapshot.params['id'];
    console.log(' this.todoId in ngOnInit is:', this.todoId)
    this.todoService.getTodos().subscribe(res=>{
      this.pendingList = new Array();
      this.approvedList = new Array();
      res.forEach((element) => {
        if (this.clientInfo.client){
          if (!element.approved && element.address == this.clientInfo.clientAddress){
            this.pendingList.push(element);
          }
          else if (element.approved && element.address == this.clientInfo.clientAddress){
            this.approvedList.push(element);
          }
        }
        else{
          if (!element.approved){
            this.pendingList.push(element);
          }
          else this.approvedList.push(element);
        }
        
      })
      this.pendingList.sort((a,b) => {return b.createdAt - a.createdAt});
      this.approvedList.sort((a,b) => {return b.createdAt - a.createdAt});
      
      // this.pendingList = res;
      console.log(this.pendingList);
      console.log(this.approvedList);
    });
  }

  parseTime(timestamp: number){
    let date = new Date(timestamp);
    let hour = '0' + date.getHours();
    let min = '0' + date.getMinutes();
    let sec = '0' + date.getSeconds();
    let year = date.getFullYear();
    let month = '0' + (date.getMonth() + 1);
    let day = '0' + date.getDate();

    return day.substr(-2) + '/' + month.substr(-2) + '/' + year + ' --- ' + hour.substr(-2) + ':' + min.substr(-2) + ':' + sec.substr(-2);
  }

  changeList(param:string){
    this.closeDetail();
    if(param == 'pending'){
      this.pending = true;
      let append = document.getElementById('pendingList');
      let approved = document.getElementById('approvedList');
      append.classList.add('active');
      approved.classList.remove('active');
    }
    else {
      this.pending = false;
      let append = document.getElementById('pendingList');
      let approved = document.getElementById('approvedList');
      append.classList.remove('active');
      approved.classList.add('active');
    }
  }
  showDetail() {
    document.getElementById('list').setAttribute('class', 'list-col half');
    setTimeout(() => {
      this.isShowDetail = true;
    }, 500);
  }

  closeDetail() {
    this.activeItemIndex = -1;
    document.getElementById('list').setAttribute('class', 'list-col full');
    this.isShowDetail = false;
  }

  selectCredential(index: number, param:boolean) {
    if (param){
      this.todoId = this.pendingList[index].id;
    }
    else this.todoId = this.approvedList[index].id;

    this.activeItemIndex = index;
  
    console.log('this.todoId in selectCredential',this.todoId)
    this.loadTodo(this.todoId).then(()=>{
      this.showDetail();
    }).catch((err)=>console.error(err))   
  }

  async approve() {
    //TODO: Send credential transaction
    // this.sendCredential();
    this.pendingList[this.activeItemIndex].approved = true;
    this.todoService.updateTodo(this.pendingList[this.activeItemIndex],this.pendingList[this.activeItemIndex].id);
    this.changeList('approved');
  }

  async cancel() {
    //TODO: Remove credential out of pending list and database
    this.todoService.removeTodo(this.todoId);
    this.closeDetail();
  }

  async sendCredential(){
    console.log("this.payloadContent",this.payloadContent);
    console.log('this.appPublicKey]',this.publicKey);
    console.log('this.sessionToken]',this.sessionToken);
    console.log('this.addres]',this.todo.address);
    // const credentials = this.pendingList[this.todoId]+this.todo.appPublicKey
    this.createTx(this.payloadContent,this.publicKey,this.sessionToken,this.todo.address);
  }

  async loadTodo(todoId) {
    const loading = await this.loadingController.create({
      message: 'Loading Todo..'
    });
    await loading.present();
 
    this.todoService.getTodo(todoId).subscribe(res => {
      loading.dismiss();
      this.todo = res;
      console.log("JSON of userData is",JSON.parse(this.todo.userData))
      this.userAddress = this.todo.address;
      console.log("-----------userAddress",this.userAddress)
      const content = JSON.parse(this.todo.userData)
      console.log("-----------content",content)
      // const appPublicKeyAndSessionToken = Object.values(this.todo)
      // console.log("-----------this.todo",this.todo)
      // console.log("-----------appPublicKeyAndSessionToken",appPublicKeyAndSessionToken)
      this.publicKey = this.todo.publicKey;
      // this.sessionToken = Object.create(JSON.parse(this.todo.publicKey))['sessionToken'];
      this.payloadContent = Object.create(content);
      console.log("-----------this.appPublicKey",this.publicKey)
      console.log("-----------this.sessionToken",this.sessionToken)
      console.log("----------- this.payloadContent", this.payloadContent)
      const credentialContent = Object.create(content).content
      console.log("-------credentialContent",content)
      const credentialID = (Object.create(content)).id
      switch (credentialID){
        case 'proximaxId':
          this.getContentNationalId(credentialContent);
          this.getPhoto();
          break;
        case 'passport':
          this.getContentPassportId(credentialContent);
          break;
        case 'employmentID':
          this.getContentEmploymentID(credentialContent);
          break;
        case 'visa':
          this.getContentVisa(credentialContent);
          break;
        case 'bankID':
          this.getContentBankID(credentialContent);
          break;
      }
    });
  }

  getContentNationalId(credentialContent:any[][]){
    this.selectedCredential.firstName = credentialContent[0][1],
    this.selectedCredential.lastName = credentialContent[1][1],
    this.selectedCredential.dateOfBirth = credentialContent[2][1],
    this.selectedCredential.address = credentialContent[3][1],
    this.selectedCredential.phone = credentialContent[4][1],
    this.selectedCredential.email = credentialContent[5][1],
    this.selectedCredential.motherName = credentialContent[6][1],
    this.selectedCredential.fatherName = credentialContent[7][1]
    this.selectedCredential.photoHash = credentialContent[8][1]
  }
  getContentEmploymentID(credentialContent:any[][]){
    this.selectedCredential.firstName = credentialContent[0][1],
    this.selectedCredential.lastName = credentialContent[1][1],
    this.selectedCredential.dateOfBirth = credentialContent[2][1],
    this.selectedCredential.address = credentialContent[3][1],
    this.selectedCredential.phone = credentialContent[4][1],
    this.selectedCredential.email = credentialContent[5][1],
    this.selectedCredential.motherName = credentialContent[6][1],
    this.selectedCredential.fatherName = credentialContent[7][1]
    this.selectedCredential.photoHash = credentialContent[8][1]
  }
  getContentPassportId(credentialContent:any[][]){
    this.selectedCredential.firstName = credentialContent[0][1],
    this.selectedCredential.lastName = credentialContent[1][1],
    this.selectedCredential.dateOfBirth = credentialContent[2][1],
    this.selectedCredential.address = credentialContent[3][1],
    this.selectedCredential.phone = credentialContent[4][1],
    this.selectedCredential.email = credentialContent[5][1],
    this.selectedCredential.motherName = credentialContent[6][1],
    this.selectedCredential.fatherName = credentialContent[7][1]
    this.selectedCredential.photoHash = credentialContent[8][1]
  }
  getContentVisa(credentialContent:any[][]){
    this.selectedCredential.firstName = credentialContent[0][1],
    this.selectedCredential.lastName = credentialContent[1][1],
    this.selectedCredential.dateOfBirth = credentialContent[2][1],
    this.selectedCredential.address = credentialContent[3][1],
    this.selectedCredential.phone = credentialContent[4][1],
    this.selectedCredential.email = credentialContent[5][1],
    this.selectedCredential.motherName = credentialContent[6][1],
    this.selectedCredential.fatherName = credentialContent[7][1]
    this.selectedCredential.photoHash = credentialContent[8][1]
  }
  getContentBankID(credentialContent:any[][]){
    this.selectedCredential.firstName = credentialContent[0][1],
    this.selectedCredential.lastName = credentialContent[1][1],
    this.selectedCredential.dateOfBirth = credentialContent[2][1],
    this.selectedCredential.address = credentialContent[3][1],
    this.selectedCredential.phone = credentialContent[4][1],
    this.selectedCredential.email = credentialContent[5][1],
    this.selectedCredential.motherName = credentialContent[6][1],
    this.selectedCredential.fatherName = credentialContent[7][1]
    this.selectedCredential.photoHash = credentialContent[8][1]
  }

  async getPhoto(){

    console.log("connectionConfig",this.connectionConfig);
    const transactionHash = this.selectedCredential.photoHash;
    const downloader = new Downloader(this.connectionConfig);
    const param = DownloadParameter.create(transactionHash).build();

    const result1 = await downloader.download(param);
    const result2 = await result1.data.getContentAsBuffer();
    const result3 = await document.createElement("img");
    result3.src = 'data:image/jpeg;base64,' + result2.toString("base64");
    this.photonew = result3.src;
    this.selectedCredential.photo = this.photonew;
  }

  async sendMessageToIpfs(msg){
    console.log('--------msg',msg)
    const upLoader = new Uploader(this.connectionConfig);
    const byteStream = new Uint8Array(Buffer.from(JSON.stringify(msg))) ;
    console.log('--------byteStream',byteStream)
    const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(byteStream,this.todoService.privateKeydAppAdmin).withTransactionMosaics([]).build();
    const result = await upLoader.upload(uploadParamsBuilder);
    return result.transactionHash
  }

  async checkMess(transactionHash){
    const downloader = new Downloader(this.connectionConfig);
    const param = DownloadParameter.create(transactionHash).build();

    const result1 = await downloader.download(param);
    console.log("********************************");
    console.log("mess from checkMess",result1);
    console.log("********************************");
  }

 async createTx(credentials:any,appPublicKey:any,sessionToken:any,address:string){
    let credential = Credentials.create(
      credentials.id,
      credentials.name,
      credentials.description,
      credentials.icon,
      credentials.documentHash,
      credentials.content,
      credentials.group,
      credentials.auth_origin,
    )

    credential.addAuthOwner(Credentials.authCreate(credential.getContent(),this.todoService.privateKeydAppAdmin));

    let mess = CredentialResponseMessage.create(
      appPublicKey,
      sessionToken,
      credential,
    );
    console.log("********************************");
    console.log("mess",mess);
    console.log("********************************");
    let messHash
    await this.sendMessageToIpfs(mess).then(
      (res)=>{
        messHash = res;
        console.log("messHash in createTx",messHash);
        
        return messHash
      }
    ).catch((err)=>console.error(err));
    
    
    console.log("messHash",messHash);
    

    let pubAcc = PublicAccount.createFromPublicKey(appPublicKey,NetworkType.TEST_NET);// account of dApp National
    let encMess = EncryptedMessage.create(messHash.toString(),pubAcc,this.todoService.privateKeydAppAdmin);
    const addressSid = Address.createFromRawAddress(address.slice(1,address.length-1))
    console.log("------------ address",addressSid)
    const transaction = TransferTransaction.create(
      Deadline.create(),
      addressSid,
      [],
      encMess,
      NetworkType.TEST_NET,
      new UInt64([0,0])
    );
    console.log('Transaction is:', transaction)
    
    // Sign Tx
    const dAppAdminAccount = Account.createFromPrivateKey(this.todoService.privateKeydAppAdmin,NetworkType.TEST_NET)
    const signedTx = dAppAdminAccount.sign(transaction,this.todoService.generationHash)
    console.log("---------- signedTx",signedTx);
    
    //  announce tx
    const nodeUrl = 'https://api-1.testnet2.xpxsirius.io'
    const resposistoryFactory = new TransactionHttp(nodeUrl);

    
    resposistoryFactory.announce(signedTx).subscribe(
      (x)=>{
        console.log(x)
      }
      ,(err)=> console.log(err)
    )
  }
}
