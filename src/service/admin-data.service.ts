import { Injectable } from '@angular/core';
import { Address, NetworkType } from 'tsjs-xpx-chain-sdk';

@Injectable({
  providedIn: 'root'
})
export class AdminDataService {

  pubickeySiriusid =''; 
  addressSiriusid = '';
  name = ''; // Name of user when him sign up
  
  /**
   * 
   */
  publicKeydApp = '785A640E8F0F976B04A8D977CDBCB645AB01413057971759FEBC62B6221630BF';
  privateKeydApp = 'B2E9947ABD626EF7AE65A4AB34DEF23E5550C8A327F2578904CB9237D364A16C';
  addressdApp: Address;
  apiNode = "";
  ws = 'wss://' + this.apiNode; //websocket
  
  /**
   * For local testing
   */
  // ws = 'ws://192.168.1.23:3000'; //local
  updateWebsocket(){
    this.ws = 'wss://' + this.apiNode;
  }
  constructor() { 
    this.addressdApp = Address.createFromPublicKey(this.publicKeydApp, NetworkType.TEST_NET);
  }
}
